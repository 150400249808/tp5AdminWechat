<?php

use think\migration\Migrator;
use think\migration\db\Column;

class PermissionCategory extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('permission_category');

        $pid = $table->hasColumn('pid');
        $path = $table->hasColumn('path');
        $sort = $table->hasColumn('sort');
        $icon = $table->hasColumn('icon');
        $url = $table->hasColumn('url');
        if(!$pid){
            $this->table('permission_category')
                ->addColumn(Column::string('pid')->setComment('上一级的id标示')->setAfter('id'))
                ->update();
        }
        if(!$path){
            $this->table('permission_category')
                ->addColumn(Column::string('path')->setComment('树形菜单的路径')->setAfter('pid'))
                ->update();
        }
        if(!$sort){
            $this->table('permission_category')
                ->addColumn(Column::integer('sort')->setComment('排序因子'))
                ->update();
        }

        if(!$icon){
            $this->table('permission_category')
                ->addColumn(Column::string('icon')->setComment('图标'))
                ->update();
        }
        if(!$url){
            $this->table('permission_category')
                ->addColumn(Column::string('url')->setComment('菜单地址'))
                ->update();
        }

    }
}
