<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AutoPageConfig extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $this->table('auto_page_config')
            ->addColumn(Column::string('title')->setComment('功能名称/菜单名称'))
            ->addColumn(Column::text('add_edit_json')->setComment('需要添加和修改的字端'))
            ->addColumn(Column::text('index_show_json')->setComment('列表页面需要显示的字段'))
            ->addColumn(Column::text('sort_show_json')->setComment('需要查询的字段'))
            ->addTimestamps()
            ->create();

    }
}
