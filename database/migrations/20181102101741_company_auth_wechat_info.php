<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CompanyAuthWechatInfo extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->table('company_auth_wechat_info')
            ->addColumn(Column::string('title')->setComment('公众号名称'))
            ->addColumn(Column::string('nick_name')->setComment('授权方昵称'))
            ->addColumn(Column::string('head_img')->setComment('授权方头像'))
            ->addColumn(Column::string('service_type_info')->setComment('授权方公众号类型，0代表订阅号，1代表由历史老帐号升级后的订阅号，2代表服务号'))
            ->addColumn(Column::string('verify_type_info')->setComment('授权方认证类型，-1代表未认证，0代表微信认证，1代表新浪微博认证，2代表腾讯微博认证，3代表已资质认证通过但还未通过名称认证，4代表已资质认证通过、还未通过名称认证，但通过了新浪微博认证，5代表已资质认证通过、还未通过名称认证，但通过了腾讯微博认证'))
            ->addColumn(Column::string('user_name')->setComment('授权方公众号的原始ID'))
            ->addColumn(Column::string('principal_name')->setComment('公众号的主体名称'))
            ->addColumn(Column::string('alias')->setComment('授权方公众号所设置的微信号，可能为空'))
            ->addColumn(Column::string('business_info')->setComment('用以了解以下功能的开通状况（0代表未开通，1代表已开通）： open_store:是否开通微信门店功能 open_scan:是否开通微信扫商品功能 open_pay:是否开通微信支付功能 open_card:是否开通微信卡券功能 open_shake:是否开通微信摇一摇功能'))
            ->addColumn(Column::string('qrcode_url')->setComment('二维码图片的URL'))
            ->addColumn(Column::string('authorization_info')->setComment('授权信息'))
            ->addColumn(Column::string('authorization_appid')->setComment('授权方appid'))
            ->addColumn(Column::string('auth_state')->setComment('unauthorized是取消授权，updateauthorized是更新授权，authorized是授权成功通知'))
            ->addColumn(Column::text('func_info')->setComment('公众号授权给开发者的权限集列表，ID为1到15时分别代表： 1.消息管理权限 2.用户管理权限 3.帐号服务权限 4.网页服务权限 5.微信小店权限 6.微信多客服权限 7.群发与通知权限 8.微信卡券权限 9.微信扫一扫权限 10.微信连WIFI权限 11.素材管理权限 12.微信摇周边权限 13.微信门店权限 14.微信支付权限 15.自定义菜单权限 请注意： 1）该字段的返回不会考虑公众号是否具备该权限集的权限（因为可能部分具备），请根据公众号的帐号类型和认证情况，来判断公众号的接口权限。'))
            ->addColumn(Column::text('miniprograminfo')->setComment('可根据这个字段判断是否为小程序类型授权'))
            ->addColumn(Column::string('authorizer_access_token')->setComment('授权方令牌'))
            ->addColumn(Column::string('expires_in')->setComment('有效期，为2小时'))
            ->addColumn(Column::string('authorizer_refresh_token')->setComment('刷新令牌'))
            ->addTimestamps()
            ->create();
    }
}
