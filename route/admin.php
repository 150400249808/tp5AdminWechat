<?php

Route::get('wl/admin/test1', 'admin/PermissionCategory/test1')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/admin/permission_category', 'admin/PermissionCategory/addEdit')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/admin/permission_category_index', 'admin/PermissionCategory/index')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/admin/permission_category_info', 'admin/PermissionCategory/info')->middleware(['auth'])->allowCrossDomain();
//--用户管理
Route::any('wl/admin/user_list', 'admin/User/index')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/admin/user_add', 'admin/User/install')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/admin/user_del', 'admin/User/delete')->middleware(['auth'])->allowCrossDomain();

Route::any('wl/admin/status/:id/:status', 'admin/User/useIdSetField')->middleware(['auth'])->allowCrossDomain();



Route::any('wl/admin/permission_category_index1', 'index/Index/test_db')->middleware(['auth'])->allowCrossDomain();

Route::any('wl/admin/pull', 'admin/System/pullAndRestart')->middleware(['auth'])->allowCrossDomain();
