<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

Route::get('think', function () {
    return 'hello,ThinkPHP5!';
});

Route::get('hello', 'index/hello')->allowCrossDomain();

Route::get('index', 'index/index')->allowCrossDomain();
Route::any('re', 'index/respon')->allowCrossDomain();

Route::get('spider', 'admin/index/spider')->allowCrossDomain();

Route::get('Api/:name', 'admin/index/:name');

Route::post('admin/Api/login', 'admin/api/login')->allowCrossDomain();

Route::controller('api','admin/Api')->middleware(['auth'])->allowCrossDomain();

Route::post('wxauthmsg', 'wechat/index/index');

Route::post('wxsendmsg/:appid', 'wechat/Index/wxsendmsg');

Route::any('upimg', 'index/upimg')->allowCrossDomain();

Route::any('query', 'query/index/index')->allowCrossDomain();
Route::any('one_page', 'query/index/onePage')->allowCrossDomain();

//Route::any('wl/menus', 'ocr/index/getMenuList')->allowCrossDomain();
Route::any('wl/menus', 'system/System/getMenuList')->allowCrossDomain();

return [

];
