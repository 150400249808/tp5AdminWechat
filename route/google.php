<?php

Route::any('wl/google/templateList', 'google/Template/index')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/google/mock', 'google/Template/mock')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/google/createRand', 'google/Template/createRand')->middleware(['auth'])->allowCrossDomain();


Route::resource('wl/google/template','google/Resource')->append(['table'=>'lyx_template']);
