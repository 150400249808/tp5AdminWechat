<?php

Route::any('ocr/getOcrBase', 'ocr/index/getOcrBase')->allowCrossDomain();

Route::any('ocr/addUser', 'ocr/index/addOcrUser')->allowCrossDomain();

Route::any('ocr/updateCount', 'ocr/index/updateOcrCount')->allowCrossDomain();

Route::any('ocr/getInfo', 'ocr/index/getPapersInfo')->allowCrossDomain();

Route::any('ocr/getUserList', 'ocr/index/getUserList')->allowCrossDomain();

Route::any('ocr/delUser', 'ocr/index/delOcrUser')->allowCrossDomain();

Route::any('ocr/getMealList', 'ocr/index/getMealList')->allowCrossDomain();

Route::any('ocr/addMeal', 'ocr/index/addMeal')->allowCrossDomain();

Route::any('ocr/delMeal', 'ocr/index/delMeal')->allowCrossDomain();

Route::any('ocr/addStatus', 'ocr/index/addStatus')->allowCrossDomain();

Route::any('ocr/getVisitAddress', 'ocr/index/getVisitAddress')->allowCrossDomain();

Route::any('ocr/getStatistics', 'ocr/index/getStatistics')->allowCrossDomain();

Route::any('ocr/getVisitList', 'ocr/index/getVisitList')->allowCrossDomain();

Route::any('ocr/getUserChargeList', 'ocr/index/getUserChargeList')->allowCrossDomain();

Route::any('ocr/getMyInfo', 'ocr/index/getMyInfo')->allowCrossDomain();



