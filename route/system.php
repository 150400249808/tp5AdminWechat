<?php

//--用户管理
//Route::group(
//    'wl/sys/',
//    function (){
//        Route::any('userList', 'system/User/index');
//        Route::any('userLogin/:username/:password', 'system/System/login');
//    }
//)->middleware(['auth'])->allowCrossDomain();

Route::any('wl/sys/userList', 'system/User/index')->middleware(['auth'])->allowCrossDomain();
Route::any('wl/sys/userLogin/:username/:password', 'system/System/login')->middleware(['auth'])->allowCrossDomain();

Route::resource('wl/sys/user','system/Resource')->append(['table'=>'sys_user'])->allowCrossDomain();

Route::resource('wl/sys/menu','system/Resource')->append(['table'=>'sys_menu'])->allowCrossDomain();

Route::resource('wl/sys/office','system/OfficeResource')->append(['table'=>'sys_office'])->allowCrossDomain();

Route::resource('wl/sys/dict','system/Resource')->append(['table'=>'sys_dict'])->allowCrossDomain();

Route::resource('wl/sys/role','system/Resource')->append(['table'=>'sys_role'])->allowCrossDomain();
