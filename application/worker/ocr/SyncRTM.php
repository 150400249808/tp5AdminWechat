<?php

/**
 * Created by PhpStorm.
 * User: wangliang
 * Date: 2019/5/10
 * Time: 3:47 PM
 */
namespace app\worker\ocr;
use app\ocr\service\Ocr;
use mikkle\tp_redis\Redis;
use think\Db;
use think\facade\Log;
use think\swoole\template\Timer;
class SyncRTM extends Timer
{
    public function initialize($arg)
    {
        // TODO: Implement _initialize() method.
    }

    public function run(){
        echo 'SyncRTM队列功能同步所有ocr识别数据从redis到mysql|||||||||||||';
        Log::record('ocr识别记录同步'.time(),'SyncRTM');
        $ocr= new Ocr();
        //放入队列lPush
        $option['index'] = "0";
        $option['port'] = "36379";
        $option['auth'] = "2015@Juyou";
        $option['host'] = "120.27.63.37";
        $cache = new Redis($option);
        //组装放入redis的数据
        $rtndata=$cache->rPop('queue');
        $array_rtndata=json_decode($rtndata,true);
        if($rtndata!=null){
            $data['id']=$ocr->uuid();
            $data['rtncode']=$array_rtndata['rtncode'];
            $data['userid']=$array_rtndata['userid'];
            $data['fullreq']=json_encode($array_rtndata['fullreq']);
            Db::connect('sync_db_config')->table('ocr_status')->insert($data);
        }
    }
}