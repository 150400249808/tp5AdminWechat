<?php

/**
 * Created by PhpStorm.
 * User: wangliang
 * Date: 2019/5/10
 * Time: 3:47 PM
 */
namespace app\worker\ocr;
use app\ocr\service\Ocr;
use mikkle\tp_redis\Redis;
use think\Db;
use think\facade\Log;
use think\swoole\template\Timer;
class SyncCountRTM extends Timer
{
    public function initialize($arg)
    {
        // TODO: Implement _initialize() method.
    }

    public function run(){
        echo 'SyncCountRTM同步请求数据从redis到mysql|||||||||||';
        Log::record('ocr识别记录同步'.time(),'SyncRTM');
        //放入队列lPush
        $option['index'] = "0";
        $option['port'] = "36379";
        $option['auth'] = "2015@Juyou";
        $option['host'] = "120.27.63.37";
        $cache = new Redis($option);
        $data=$cache->searchKeys("ocr_"."*");
        foreach ($data as $eachdata){
            $count_data=json_decode($cache->get($eachdata),true);
            $res_count=$count_data["count"];
            $userid=$count_data["userid"];
            //更新到user_base的剩余数量中
            $bool = Db::connect('sync_db_config')
                ->table('ocr_base')
                ->where('userid', $userid)
                ->setField('res_count', $res_count);
        }
        //组装放入redis的数据
//        $rtndata=$cache->hVals('ocr_ocr_bdd87028-855b-ab3f-ab78-bef89441e094');
//        dump($rtndata);
    }
}