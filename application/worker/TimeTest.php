<?php

/**
 * Created by PhpStorm.
 * User: wangliang
 * Date: 2019/5/10
 * Time: 3:47 PM
 */
namespace app\worker;
use think\facade\Log;
use think\swoole\template\Timer;
class TimeTest extends Timer
{
    public function initialize($arg)
    {
        // TODO: Implement _initialize() method.
    }

    public function run(){
        echo '执行测试定时任务';
        Log::record('定时任务执行日志'.time(),'TimeTest');
    }
}