<?php


namespace app\ocr\validate;

use think\Validate;

class Index extends Validate
{

    protected $rule = [
        'user_name' => 'require|alphaNum',
        'password'  => 'require',
        'count'  => 'require|number',
    ];

    protected $message = [
        'user_name.require'  => '请输入账号',
        'user_name.alphaNum' => '账号只能是数字和字母',
        'password.require'   => '请输入密码',
        'count'   => '数量只能是数字',
    ];
    protected $scene = [
        'login'    => ['user_name', 'password'],
        'register' => ['user_name', 'password'],
        'addocruser'=>['count']
    ];



}