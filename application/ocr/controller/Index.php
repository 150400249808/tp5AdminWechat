<?php


namespace app\ocr\controller;

use app\ocr\service\Ocr;
use think\facade\Request;
use app\base\controller\base;

class Index extends base
{

    /*
     * 新增OCR识别用户
     * 插入ocr_base表和ocr_detail表
     */
    public function addOcrUser(Request $request)
    {
        $data = $request::param();
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->addOcrUser($data),'success'));
    }

    /*
    * 查询用户列表
    */
    public function getUserList()
    {
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->getUserList(),'success'));
    }

    /*
    * 查询套餐列表
    */
    public function getMealList()
    {
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->getMealList(),'success'));
    }

    /*
    * 模拟请求
    */
    public function getVisitAddress(Request $request)
    {
        $userid = $request::param("userid");
        $ocr = new Ocr();
        return $ocr->getVisitAddress($userid);
    }

    /*
    * 添加套餐
    */
    public function addMeal(Request $request)
    {
        $data = $request::param();
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->addMeal($data),'success'));
    }

    /*
    * 删除套餐
    */
    public function delMeal(Request $request)
    {
        $data = $request::param();
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->delMeal($data),'success'));
    }

    /*
    * 删除OCR识别用户
    * 删除ocr_base表和ocr_detail表
     * 删除redis数据
     * 逻辑删除
    */
    public function delOcrUser(Request $request)
    {
        $userid = $request::param("userid");
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->delOcrUser($userid),'success'));
    }

    /*
     * OCR用户充值 充值可冲正负
     * 插入ocr_detail表,状态为未充值
     */
    public function updateOcrCount(Request $request)
    {
        $data = $request::param();
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->updateOcrCount($data),'success'));
    }

    /*
     * 请求调用OCR识别
     * 返回后插入redis和队列
     * redis更新次数，每隔两分钟自动同步到ocr_base
     * 1、判断用户身份
     * 2、判断数量->判断redis是否有值、没有的话查询数据库插入一条，有的话直接更新
     * 3、如果redis和数据库记录都为0则查询充值记录redis 补充进去，如果都没有则抛异常余额不足
     * 队列放入请求状态，自动同步到ocr_status中
     */
    public function getPapersInfo(Request $request)
    {
        $file = $request::param("file");
        $userid = $request::param("userid");
        $checksec = $request::param("secret");
        $ocr = new Ocr();
        return $ocr->ocrIdcard($file, $checksec, $userid);
    }

    /*
     * 获取统计数据
     */
    public function getStatistics(Request $request)
    {
        $userid = $request::param("userid");
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->getStatistics($userid),'success'));
    }

    /*
     * 获取识别列表
     */
    public function getVisitList(Request $request)
    {
        $userid = $request::param("userid");
        $pageno = $request::param("pageno");
        $ocr = new Ocr();
        $rtn=$ocr->getVisitList($pageno,$userid);
        $rtndata=$rtn["datasource"];
        $rtncount=$rtn["allcount"][0]["count"];
        return json($this::returnPageCode(1001,'success',$rtncount,10,$rtndata));
    }

    /*
     * 获取充值列表
     */
    public function getUserChargeList(Request $request)
    {
        $userid = $request::param("userid");
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->getUserChargeList($userid),'success'));
    }

    /*
     * 获取个人信息接口（用户侧）
     */
    public function getMyInfo(Request $request)
    {
        $userid = $request::param("userid");
        $ocr = new Ocr();
        return json($this::returnCode(1001,$ocr->getMyInfo($userid),'success'));
    }

    /*
      * 获取菜单
      */
    public function getMenuList(Request $request)
    {
         $menu = "{errcode: 200,
        errmsg: 'ok',
        data: [
          {
            id: 29,
            title: '新建',
            path: '',
            icon: 'ios-keypad',
            children: [
              { title: 》'新建用户', path: '/create-user', id: 30, icon: 'ios-keypad' },
              { title: '用户管理', path: '/user-list', id: 31, icon: 'ios-keypad' },
              { title: '套餐设置', path: '/meal-list', id: 32, icon: 'ios-keypad' },
              { title: '统计', path: '/statistics', id: 33, icon: 'ios-keypad' },
              { title: '模版管理', path: 'account', id: 34, icon: 'ios-keypad' }
            ]
          }
        ]}";

         $option[]=['title'=>'新建用户','path'=>'/create-user','id'=>'30','icon'=>'ios-keypad'];
         $option[]=['title'=>'用户管理','path'=>'/user-list','id'=>'31','icon'=>'ios-keypad'];
         $option[]=['title'=>'套餐设置','path'=>'/meal-list','id'=>'32','icon'=>'ios-keypad'];
         $option[]=['title'=>'统计','path'=>'/statistics','id'=>'33','icon'=>'ios-keypad'];
         $option[]=['title'=>'模版管理','path'=>'account','id'=>'34','icon'=>'ios-keypad'];

         $data["id"]=29;
         $data["title"]='新建';
         $data["path"]='';
         $data["icon"]='ios-keypad';
         $data["children"]=$option;

        return $this::returnCode(1001,[$data],'succesful');

    }

}