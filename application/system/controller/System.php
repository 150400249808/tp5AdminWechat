<?php

namespace app\system\controller;

use app\base\controller\base;
use think\Db;
use think\Config;
use think\facade\Env;
use think\facade\Request;
use app\system\model\User as UserModel;

class System extends base
{
    public function pullAndRestart()
    {
        $name = request()->param();
        $path = Env::get('root_path');
        $file_path = $path.'test.sh';
        if(file_exists($file_path)){
            exec($file_path,$output);
            echo $file_path;
            return $this->returnJson(1001,'成功',$output);
        }else{
            echo 'no';
            return $this->returnJson(1002,'没有找到文件');
        }

    }


    public function login(Request $request){
        $userName = $request::param('username');
        $password = $request::param('password');
        $mo = new UserModel();
        $ret = $mo->login($userName,$password);
        if($ret != false) {
           return $this->returnJson(1001,'登录成功',['key'=>'3434343434']);
        }else{
           return $this->returnJson(1001,'用户名密码错误','3434343434');
        }
    }

    public function getMenuList(Request $request)
    {

        $option[]=['title'=>'模版管理','path'=>'/google/template','id'=>'30','icon'=>'ios-keypad'];
        $option[]=['title'=>'用户查询','path'=>'/google/user-search','id'=>'31','icon'=>'ios-keypad'];

        $data["id"]=29;
        $data["title"]='系统管理';
        $data["path"]='';
        $data["icon"]='ios-keypad';
        $data["children"]=$option;

        return $this->returnJson(1001,'succesful',[$data]);

    }

}
