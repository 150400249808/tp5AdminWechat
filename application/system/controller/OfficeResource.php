<?php

namespace app\system\controller;

use think\facade\Request;
use app\base\controller\base;
use app\system\model\OfficeResource as OfficeResourceModel;

class OfficeResource extends base
{

    protected $model;
    protected $param;
    protected $pageSize;

    public function __construct()
    {
        $this->model = new OfficeResourceModel();
        $this->param = Request::param();
        $this->pageSize = !empty($this->param['pageSize']) ? $this->param['pageSize'] : 10;
    }

    /**
     * @param Request $request
     *
     * @return \think\Response
     * @throws \think\exception\DbException
     */
    public function index(Request $request)
    {
        $tableInfo = getTableHeader('office');
        $list = $this->model->field($tableInfo[1])->where('del_flag', '=', 0)->paginate($this->pageSize, false, [])->toArray();
        $list['header'] = $tableInfo[0];
        return $this->returnPageJson(1001, '返回数据成功', $list);
    }


    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     *
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $this->param;
        unset($data['table']);
        $ret = $this->model->editData($data, $this->param['table']);
        return $this->returnJson($ret['code'], $ret['msg']);
    }

    /**
     * @param $id
     *
     * @return \think\Response
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function read($id)
    {
        $tableInfo = getTableHeader('office');
        $info = $this->model->field($tableInfo[1])->where('id', '=', $id)->find();
        if ($info) {
            return $this->returnJson(1001, '存在数据', $info);
        } else {
            return $this->returnJson(1001, '没有查询到');
        }
    }



    /**
     * @param $id
     *
     * @return \think\Response
     * @throws \Exception
     */
    public function delete($id)
    {

        $result = $this->model->whereIn('id', json_decode($this->param['ids']))->setField('del_flag', 1);
        return $result ? $this->returnJson(1001, '删除成功' . $id) : $this->returnJson(1002, '删除失败' . $id);
    }


}
