<?php


namespace app\admin\controller;

use app\base\controller\base;
use think\Request;
use think\facade\Debug;

class PermissionCategory extends base
{

    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    public function test1()
    {
        $data = ['list' => 1, 'list2' => 3];

        throw new \think\Exception('异常消息', 10006);
        return json($this->returnCode('10001', $data, '返回的真实数据'));
    }

    public function addEdit(Request $request)
    {
        $params = $request->param();
        $mo = new \app\admin\model\PermissionCategory();
        $ret = $mo->editData($params);
        return json($this->returnCode($ret['code'], '', $ret['msg']));
    }

    public function info(Request $request){
        $id = $request->param('id');
        $mo = new \app\admin\model\PermissionCategory();
        $info = $mo->where('id','=',$id)->find();
        return json($this->returnCode(1001,$info,'返回数据成功'));
    }

    public function index()
    {
        $mo = new \app\admin\model\PermissionCategory();
        $list = $mo->order("bpath asc,sort desc")->field("id,pid,name as title,sort,status,url,icon,concat(path,'-',id) as bpath")->select()->toArray();
        $result = build_tree($list, 0);
        return json($this->returnCode(1001, $result, '返回数据成功'));
    }

}