<?php

namespace app\system\controller;

use think\facade\Request;
use app\base\controller\base;
use app\system\model\Resource as ResourceModel;

class Resource extends base
{

    protected $model;
    protected $param;
    protected $pageSize;

    public function __construct()
    {
        $this->model = new ResourceModel();
        $this->param = Request::param();
        $this->pageSize = !empty($this->param['pageSize']) ? $this->param['pageSize'] : 10;
    }

    /**
     * @param Request $request
     *
     * @return \think\Response
     * @throws \think\exception\DbException
     */
    public function index(Request $request)
    {
        $table = $this->param['table'];
        $tableInfo = getTableHeader($table);
        $whereMust = empty($tableInfo[2]) ? []:$tableInfo[2];

        $list = $this->model->table($table)->where($whereMust)->field($tableInfo[1])->paginate($this->pageSize, false, [])->toArray();
        $list['header'] = $tableInfo[0];
        return $this->returnPageJson(1001, '返回数据成功', $list);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     *
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $this->param;
        unset($data['table']);
        $ret = $this->model->editData($data,$this->param['table']);
        return $this->returnJson($ret['code'],$ret['msg']);
    }

    /**
     * 显示指定的资源
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function read($id)
    {
        $tableInfo = getTableHeader($this->param['table']);
        $info = $this->model->table($this->param['table'])->field($tableInfo[1])->where('id', '=', $id)->find();
        if ($info) {
            return $this->returnJson(1001, '存在数据', $info);
        } else {
            return $this->returnJson(1001, '没有查询到');
        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     *
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function delete($id)
    {
        $table = $this->param['table'];
        $result = $this->model->table($table)->whereIn('id', json_decode($this->param['ids']))->setField('del_flag', 1);
//        $result = $this->model->table($this->param['table'])->whereIn('id',json_decode($this->param['ids']))->useSoftDelete('delete_time',time())->delete();
        return $result ? $this->returnJson(1001, '删除成功'.$id) : $this->returnJson(1002, '删除失败'.$id);
    }
}
