<?php

namespace app\system\model;

use app\base\model\Base;
use mikkle\tp_tools\Rand;

class OfficeResource extends Base
{
    protected $insert = ['id'];
    protected $table = 'sys_office';
    protected $auto = ['parent_ids', 'create_date', 'del_flag'];

    protected function setIdAttr()
    {
        return Rand::createNonceStr(32);
    }

    protected function setParentIdsAttr($value, $data)
    {
        $pid = empty($data['parent_id']) ? 0 : $data['parent_id'];
        $mi = $this->field('id,parent_ids')->where('id', '=', $pid)->find();
        $path = $pid != 0 ? $mi['parent_ids'] . ',' . $mi['id'] : 0;
        return $path;
    }

    public function setCreateDateAttr($value)
    {
        return toDate(time());
    }

    public function setDelFlagAttr($value)
    {
        return 0;
    }
}
