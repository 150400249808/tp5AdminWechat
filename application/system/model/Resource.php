<?php

namespace app\system\model;

use app\base\model\Base;
use mikkle\tp_tools\Rand;
class Resource extends Base
{
    protected $insert = ['id'];
    protected $pk = 'id';

    protected function setIdAttr(){
        return Rand::createNonceStr(32);
    }
}
