<?php

namespace app\system\model;

use app\base\model\Base;

/**
 * Class User
 * @package app\system\model
 */
class User extends Base
{
    protected $autoWriteTimestamp = 'datetime';
    protected $createTime = 'create_date';
    protected $updateTime = 'update_date';

    public function getLastLoginTimeAttr($value, $data)
    {
        if ($data['last_login_time'] > 0) {
            return toDate($data['last_login_time']);
        } else {
            return '';
        }
    }

    /**
     * 给数据表做的软删除
     */
    public function delFlag($id = [], $key = 'id')
    {
        return $this->whereIn($key, $id)->setField('del_flag', 1);
    }

    public function login($username, $password)
    {
        $ret = $this::where('login_name', $username)->where('password', $password)->find();
        return $ret ? $ret : false;
    }

}