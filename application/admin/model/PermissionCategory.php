<?php


namespace app\admin\model;

use app\base\model\Base;
class PermissionCategory extends Base
{

    protected $auto = ['path','create_time'];

    public function setPathAttr($value,$data)
    {
        $pid=$data['pid'];
        $mi=$this->field('id,path')->where('id','=',$pid)->find();
        $path=$pid!=0?$mi['path'].'-'.$mi['id']:0;
        return $path;
    }

    public function setCreateTimeAttr($value){
        return time();
    }
}