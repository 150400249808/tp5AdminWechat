<?php


namespace app\admin\model;

use app\base\model\Base;
use think\model\concern\SoftDelete;
class User extends Base
{
    use SoftDelete;
    protected $autoWriteTimestamp = true;
    protected $deleteTime = 'delete_time';

    protected $defaultSoftDelete = 0;

    public function getLastLoginTimeAttr($value, $data)
    {
        if($data['last_login_time'] >0){
            return toDate($data['last_login_time']);
        }else{
            return '';
        }
    }
}