<?php
namespace app\admin\controller;
use think\Db;
use think\Config;
use app\base\controller\base;
use gmars\rbac\Rbac;
class RbacAuth extends base
{


    public function index()
    {
        $name = request()->param('name');
        $a = config();
        dump($a);
        return json(array('status'=>1,'msg'=>'你好'.$name));
    }

    public function adduser(){
        $rbacObj = new Rbac();
        $data = ['user_name' => 'zhangsan', 'status' => 1, 'password' => md5('zhangsan')];
        $rbacObj->createUser($data);
    }


    public function addRole(){
        $rbacObj = new Rbac();
        $data = [
            'name' => '商品列表',
            'status' => 1,
            'description' => '查看商品的所有列表',
            'path' => '/index/goods/list',
            'create_time' => time()
        ];
        $rbacObj->createPermission($data);

    }

    public function addGroup(){
        $rbacObj = new Rbac();
        $data = [
            'name' => '商品管理员2',
            'status' => 1,
            'description' => '商品管理员负责商品的查看修改删除等操作',
            'sort_num' => 10,
        ];
        $rbacObj->createRole($data);

    }

    public function addUserRole(){
        $rbacObj = new Rbac();
        $rbacObj->assignUserRole(1, [1, 2]);
    }

    public function addqx(){
        $rbacObj = new Rbac();
        $rbacObj->assignRolePermission(1, [1]);

    }

    public function moveRole(){
        $rbacObj = new Rbac();
        $rbacObj->moveRole(1,2);
    }

}
