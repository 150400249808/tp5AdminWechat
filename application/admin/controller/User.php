<?php


namespace app\admin\controller;

use app\base\controller\base;
use think\Db;
use think\facade\Request;
use think\facade\Debug;
use app\admin\model\User as UserModel;
use mikkle\tp_master\Exception;

class User extends base
{

    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    public function index()
    {
        $fields[0] = ['id' => 'id', 'user_name' => '用户名', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
        $fields[1] = ['id', 'user_name', 'mobile', 'create_time', 'last_login_time', 'status'];

        $list = UserModel::field($fields[1])->paginate(10, false, [])->toArray();
        $list['header'] = $fields[0];
        return $this->returnPageJson(1001, '返回数据成功', $list);
    }

    public function install()
    {
        $data['user_name'] = 'test1';
        $data['password'] = '123456';
        $data['mobile'] = '15040249808';
        $all = [];
        for ($i = 0; $i < 10; $i++) {
            $all[] = $data;
        }
        $mo = new UserModel();
        $ret = $mo->saveAll($all);
        return json($ret);
    }

    /**
     * 软删除
     * @param Request $request
     *
     * @return \think\Response
     * @throws Exception
     */
    public function delete(Request $request)
    {
        $user_id = $request::param('id');
        if (empty($user_id)) {
            throw new Exception("请传递要删除的id");
        }
        $result = UserModel::destroy(json_decode($user_id));
        return $result ? $this->returnJson(1001, '删除成功'.$user_id) : $this->returnJson(1002, '删除失败'.$user_id);

    }

    public function useIdSetField(Request $request){
        $data = $request::param();
        $mo = new UserModel();
        $ret = $mo->editData($data);
        return $this->returnJson($ret['code'],$ret['msg']);
    }
}