<?php

namespace app\google\controller;

use PhpParser\Node\Expr\Cast\Object_;
use think\App;
use think\Controller;
use think\facade\Request;
use app\base\controller\base;
use app\google\model\Resource as ResourceModel;

class Resource extends base
{

    protected $model;
    protected $param;
    protected $pageSize;

    public function __construct(App $app = null)
    {
        $this->model = new ResourceModel();
        $this->param = Request::param();
        $this->pageSize = !empty($this->param['pageSize']) ? $this->param['pageSize'] : 10;
    }

    /**
     * @param Request $request
     *
     * @return \think\Response
     * @throws \think\exception\DbException
     */
    public function index(Request $request)
    {
        $fields[0] = ['id' => 'id', 'user_name' => '用户名', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
        $fields[1] = ['id', 'title', 'content', 'create_time','is_site'];
        $list['header'] = $fields[0];

        $table = $this->param['table'];
        $list = $this->model->table($table)->field($fields[1])->paginate($this->pageSize, false, [])->toArray();
        return $this->returnPageJson(1001, '返回数据成功', $list);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param \think\Request $request
     *
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $this->param;
        unset($data['table']);
        $ret = $this->model->editData($data,$this->param['table']);
        return $this->returnJson($ret['code'],$ret['msg']);
    }

    /**
     * 显示指定的资源
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param \think\Request $request
     * @param int $id
     *
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     *
     * @return \think\Response
     */
    public function delete($id)
    {

        $result = $this->model->table($this->param['table'])->whereIn('id',json_decode($this->param['ids']))->useSoftDelete('delete_time',time())->delete();
        return $result ? $this->returnJson(1001, '删除成功'.$id) : $this->returnJson(1002, '删除失败'.$id);
    }
}
