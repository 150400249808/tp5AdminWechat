<?php


namespace app\google\model;

use app\base\model\Base;
use think\model\concern\SoftDelete;
class Template extends Base
{
    use SoftDelete;
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

}