<?php

namespace app\base\controller;

use think\Controller;
use think\facade\Request;
use app\base\controller\base;
use app\google\model\Resource as ResourceModel;
class Resource extends base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $fields[0] = ['id' => 'id', 'user_name' => '用户名', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
        $fields[1] = ['id', 'title', 'content', 'create_time'];
        $list['header'] = $fields[0];

        $pageSize = $request::param('pageSize',10);
        $mo =  new ResourceModel(['table'=>'lyx_template']);
        $list = $mo::field($fields[1])->paginate($pageSize, false, [])->toArray();
        return $this->returnPageJson(1001, '返回数据成功', $list);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
