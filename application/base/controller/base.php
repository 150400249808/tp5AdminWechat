<?php
/**
 * Created by PhpStorm.
 * User: wangliang
 * Date: 2018/9/26
 * Time: 下午8:03
 */

namespace app\base\controller;

use app\admin\controller\Api;
use think\Controller;
use think\facade\Config;
use mikkle\tp_tools\ApiCode;
use think\facade\Request;
use think\Response;
use think\Db;

/**
 * Class base
 * @package app\base\controller
 */
abstract class base extends controller
{

    /**
     * 仅仅是返回一个固定格式的数组，控制器里面需要json函数返回给页面
     *
     * @param string $code
     * @param array $data
     * @param string $msg
     * @param array $append
     *
     * @return array
     */
    public function returnCode($code = '', $data = [], $msg = '', array $append = [])
    {
        return ApiCode::jsonCode($code, $data, $msg, $append);
    }

    /**
     * 直接返回json对象给前端，控制器直接return 这个结果就可以
     *
     * @param string $code
     * @param array $data
     * @param string $msg
     * @param array $append
     *
     * @return Response
     */
    public function returnJson($code = '', $msg = '', $data = [], array $append = [])
    {
        $rdata = ApiCode::jsonCode($code, $data, $msg, $append);
        return Response::create($rdata, 'json', 200, [], []);
    }

    /**
     * 需要建立模型，模型分页返回的结果放到data里面，直接return结果给前端
     *
     * @param string $code
     * @param string $msg
     * @param array $data
     * @param array $append
     *
     * @return Response
     */
    public function returnPageJson($code = '', $msg = '', $data = [], array $append = [])
    {
        $params = Request::param();
        $returnData['results'] = $data['data'];
        $returnData['totalPage'] = $data['last_page']; //总页数
        $returnData['pageSize'] = $data['per_page'];
        $returnData['totalRecord'] = $data['total'];
        $returnData['pageNo'] = $data['current_page'];
        if (!empty($data['header'])) {

            $returnData['header'] = $data['header'];
        }
        $returnData['params'] = $params;
        $rdata = ApiCode::jsonCode($code, $returnData, $msg, $append);
        return Response::create($rdata, 'json', 200, [], []);
    }

    /**
     * 自定义分页返回给前端数据，控制器需要用json函数返回
     *
     * @param string $code 返回编码
     * @param string $msg 返回的信息
     * @param $total    总页数
     * @param $paginate 每页数量
     * @param $data 当前数据
     * @param array $append 其他数据
     *
     * @return array
     */
    public function returnPageCode($code = '', $msg = '', $total, $paginate, $list, array $append = [])
    {
        $params = Request::param();
        $data['pageNo'] = !empty($params['page']) ? $params['page'] : 1;
        unset($params['page']);
        $data['totalPage'] = ceil($total / $paginate); //总页数
        $data['pageSize'] = $paginate;
        $data['totalRecord'] = $total;
        $data['params'] = $params;
        $data['results'] = $list;

        return ApiCode::jsonCode($code, $data, $msg, $append);
    }


    /**
     * @param $table
     * @param $fields
     * @param $where
     *
     * @return array
     * @throws \think\exception\DbException
     */
    public function oneTableJson($table, $fields, $where)
    {
        $header = $fields[0];
        $field = $fields[1];
        $list = Db::name($table)->where($where)->field($field)->paginate(10)->select();
        return ['header' => $header, 'list' => $list];
    }

    public function _empty(){
        return $this->returnJson(10010,'不存在方法');
    }

}