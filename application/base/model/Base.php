<?php
/**
 * Created by PhpStorm.
 * User: wangliang
 * Date: 2018/9/26
 * Time: 下午7:52
 */

namespace app\base\model;

use think\Model;

abstract class Base extends Model
{
    /**
     * 根据有Id修改信息 无Id 新增信息
     * #User: Mikkle
     * #Email:776329498@qq.com
     * #Date:
     *
     * @param $data
     *
     * @return false|int|string
     * @throws
     */
    public function editData($data, $table = false)
    {
        if ($table != false) {
            $this->table = $table;
        }
        if (!empty($data['id'])) {
            $id = $data['id'];
            unset($data['id']);
            $save = $this->allowField(true)->save($data, ['id' => $id]);
        } else {
            unset($data['id']);
            $save = $this->allowField(true)->save($data);
        }
        if ($save == 0 || $save == false) {
            $res = ['code' => 1009, 'msg' => '数据更新失败',];
        } else {
            $res = ['code' => 1001, 'msg' => '数据更新成功',];
        }
        return $res;
    }
}