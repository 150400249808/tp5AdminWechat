<?php

namespace app\base\model;

use app\base\model\Base;
use think\model\concern\SoftDelete;
class Resource extends Base
{
    use SoftDelete;
    protected $autoWriteTimestamp = true;
    protected $createTime = 'create_time';
    protected $updateTime = 'update_time';
    protected $deleteTime = 'delete_time';
    protected $defaultSoftDelete = 0;

    protected $pk = 'id';


}
