<?php

namespace app\base\exception;


use Exception;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\ValidateException;

class WlHandle extends Handle
{
    public function render(Exception $e)
    {
        // 参数验证错误
        if ($e instanceof ValidateException) {
            return json($e->getError(), 422);
        }

        // 请求异常
        if ($e instanceof HttpException && request()->isAjax()) {
            return response($e->getMessage(), $e->getStatusCode());
        }

        $data = [
            'errcode' => intval($e->getCode()) ?: -1,
            'errmsg'  => $e->getMessage(),
        ];
        //因本项目是API接口开发 统一返回固定格式
//        return json($data);
        // 其他错误交给系统处理
        return parent::render($e);
    }

}