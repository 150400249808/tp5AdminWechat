<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * @param $time
 * @param string $format
 *
 * @return false|string
 */
function toDate($time, $format = 'Y-m-d H:i:s')
{
    if (empty($time)) {
        return '';
    }
    $format = str_replace('#', ':', $format);
    return date($format, $time);
}
//树形结构页面函数
function build_tree($rows, $root_id)
{
    $childs = findChild($rows, $root_id);
    if (empty($childs)) {
        return null;
    }
    foreach ($childs as $k => $v) {
        $rescurTree = build_tree($rows, $v['id']);
        if (null != $rescurTree) {
            $childs[$k]['childs'] = $rescurTree;
        }
    }
    return $childs;
}
/*
 * 递归无限菜单
 */
function findChild(&$arr, $id)
{
    $childs = array();
    foreach ($arr as $k => $v) {
        if ($v['pid'] == $id) {
            $childs[] = $v;
        }
    }
    return $childs;
}

function getTableHeader($key){
    $user_header = ['id' => 'id', 'company_id' => '归属公司', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
    $user_fields = ['id', 'company_id', 'office_id', 'login_name','no','name','email','phone','mobile','user_type','photo','create_date','login_flag'];
    $where_must = ['del_flag'=>0];

    $menu_header = ['id' => 'id', 'company_id' => '归属公司', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
    $menu_fields = ['id', 'parent_id', 'parent_ids', 'name','type','sort','href','target','icon','href1','target1','icon1','href2','target2','icon2','is_show','menu_permission','btn_permission','interface_permission','remarks'];
    $menu_where_must = ['del_flag'=>0];


    $dict_header = ['id' => 'id', 'company_id' => '归属公司', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
    $dict_fields = ['id', 'office_id', 'name', 'enname','role_type','data_scope','is_sys','useable','create_by','create_date','update_by','update_date','remarks','auth_start','auth_end'];
    $dict_where_must = ['del_flag'=>0];

    $role_header = ['id' => 'id', 'company_id' => '归属公司', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
    $role_fields = ['id', 'value', 'label', 'type','type_category','description','sort','parent_id','remarks','dict_type'];
    $role_where_must = ['del_flag'=>0];

    $office_header = ['id' => 'id', 'company_id' => '归属公司', 'mobile' => '手机号', 'create_time' => '创建时间', 'last_login_time' => '最后登录时间', 'status' => '状态'];
    $office_fields = ['id', 'parent_id', 'parent_ids', 'name','sort','area_id','code','type','grade','address','zip_code','master','phone','fax','email','useable','primary_person','deputy_person','create_date','remarks'];

    $data['sys_user'] = [$user_header,$user_fields,$where_must];
    $data['sys_menu'] = [$menu_header,$menu_fields,$menu_where_must];
    $data['office'] = [$office_header,$office_fields];
    $data['sys_dict'] = [$dict_header,$dict_fields,$dict_where_must];
    $data['sys_role'] = [$dict_header,$dict_fields,$dict_where_must];
    return $data[$key];
}