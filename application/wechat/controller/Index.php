<?php

namespace app\wechat\controller;

use app\base\controller\base;
use think\facade\Cache;
use mikkle\tp_wechat\support\StaticFunction;
use mikkle\tp_wechat\support\Prpcrypt;
use think\facade\Config;
use mikkle\tp_wechat\base\CompanyWechatBase;
use mikkle\tp_wechat\WechatCompanyApi;
use mikkle\tp_wechat\support\Curl;

class Index extends base
{
    /**
     * 授权事件接收URL
     */
    public function index()
    {
        $wxchat = new WechatCompanyApi();
        $wxchat->acceptComponentVerifyTicket();
    }

    /**
     * 消息与事件接收URL
     */
    public function wxsendmsg()
    {
        $wbApi = new WechatCompanyApi();
        $wbApi->index();


//        $data =  $wbApi->msgEncrypt();//消息解密

//      下面的是测试消息回复功能
//        $text = str_replace(array("\r\n", "\r"), "\n", '你好谢谢');
//        $textTpl = '<xml>
//					<ToUserName><![CDATA[%s]]></ToUserName>
//					<FromUserName><![CDATA[%s]]></FromUserName>
//					<CreateTime>%s</CreateTime>
//					<MsgType><![CDATA[%s]]></MsgType>
//					<Content><![CDATA[%s]]></Content>
//					<FuncFlag>0</FuncFlag>
//					</xml>';
//        $resultStr = sprintf($textTpl, 'oZ9t5uKG_RYntxdSQXRuE24w4L84', 'gh_91a07ce950f2', time(), 'text', $text);

//        $wbApi = new WechatCompanyApi();
//        $data =  $wbApi->msgDecrypt($resultStr);

//        echo $data;

    }


    public function getTicket()
    {
        $data = cache('component_verify_ticket');
        dump($data);

        $str = '<xml>
<AppId>32323 </AppId>
<CreateTime>1413192605 </CreateTime>
<InfoType> 1111</InfoType>
<ComponentVerifyTicket>323 </ComponentVerifyTicket>
</xml>';

        dump(StaticFunction::xml2arr($str));

        //
        //        $postData = $this->request->post();
        //        dump(StaticFunction::xml2arr($postData));

        $xx = cache('aabb');
        $data = StaticFunction::xml2arr($xx);
        dump($data);

        $pr = new Prpcrypt('ESiYzDeS8BYAIPIj7jYjisDij86Z6iSSYYyys3Daymj');
        $msg = $pr->decrypt($data['Encrypt'], $data['AppId']);
        dump($msg);
        dump(StaticFunction::xml2arr($msg[1]));


    }


    function getToken()
    {
        $data["appid"] = 'wxa09e5355d8a4c635';
        $data["appsecret"] = 'e416387ea25ca4c86182dad63d3b5144';
        $data["encodingAesKey"] = 'ESiYzDeS8BYAIPIj7jYjisDij86Z6iSSYYyys3Daymj';
        $wxchat = new CompanyWechatBase($data);
        $ret = $wxchat->getAuthCode();
        dump($ret);
    }

    function getCo()
    {
        $data = Config::get("wechat.company_wechat.default_options_name");
        dump($data);
    }

    /**
     * 进行授权
     */
    function authRun()
    {
        $wxchat = new WechatCompanyApi();
        $url = $wxchat->getAuthUrl(3, 'https://txy5.sylyx.cn/wechat/index/resultAuth');
        $this->redirect($url);
    }

    /**
     * 授权回调函数
     */
    function resultAuth()
    {
        $wxchat = new WechatCompanyApi();
        $result = $wxchat->saveApiQueryAuth();
        if ($result['code'] == 'T') {
            echo '授权成功';
        } else {
            echo 'faile,失败了【' . $result['msg'] . '】';
        }
    }

    /**
     * 更新用户的基础信息到数据库
     */
    public function getCompanyInfo()
    {
        $wxchat = new WechatCompanyApi();
        $result = $wxchat->apiGetAuthorizerInfo("wxf6b27cb2c4c5dda3");
        if ($result['code'] == 'T') {
            echo '授权成功';
        } else {
            echo 'faile,失败了【' . $result['msg'] . '】';
        }
    }


    public function siSign()
    {
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

        $getData = $this->request->get();

        dump($getData);

        //TODO 对微信来的数据进行 签名认证-不符合规则不存入本地
        $toData = StaticFunction::xml2arr($postStr);
        dump($toData);
        $pr = new \mikkle\tp_wechat\support\Prpcrypt('ESiYzDeS8BYAIPIj7jYjisDij86Z6iSSYYyys3Daymj', 'star409964901');
        $msg = [];
        $ret = $pr->decryptMsg($getData['msg_signature'], $getData['timestamp'], $getData['nonce'], $toData, $msg);
        dump($msg);
        dump($ret);

    }

    /**
     *
     */
    public function abc()
    {
        $options = Cache::get('jie');
        $options2 = Cache::get('posxxx');
        $options3 = Cache::get('posxxx2');
        $options4 = Cache::get('posxxx3');
        $options5 = Cache::get('posxxx4');
        $options6 = Cache::get('save_1');
//        $options['toDate'] = toDate($options['timestamp']);
        dump($options);
//        dump ($options2);
//        dump($options3);
//        dump($options4);
//        dump($options5);
//        dump($options6);

        /*调用微信功能测试没有问题*/
//        $cwb = new CompanyWechatBase();
//        $token = $cwb->getComponetToken('wxf6b27cb2c4c5dda3');
//        $url = 'https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=' . $token;
//        $result = Curl::curlGet($url);
//        $res = json_decode($result, TRUE);
//        dump($res);
        /*调用微信功能测试没有问题*/

    }

    function getRandomStr()
    {

        $str = "";
        $str_pol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($str_pol) - 1;
        for ($i = 0; $i < 16; $i++) {
            $str .= $str_pol[mt_rand(0, $max)];
        }
        return $str;
    }
}
