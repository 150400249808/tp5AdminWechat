<?php


namespace app\wechat\controller;

use mikkle\tp_aliyun\IdcardOcr;
class AliOcr
{
    public function ocr(){
        $ocr = new IdcardOcr();
        $ocr->ocrIdcard();
        return json($ocr->ocrIdcard());
    }
}