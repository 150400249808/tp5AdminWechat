<?php

namespace app\index\controller;

use app\base\controller\base;
use think\Db;
use think\facade\Cache;
use think\facade\Debug;
use think\facade\Request;
use GuzzleHttp\Client;
use mikkle\tp_tools\Curl;
use think\swoole\queue\Task;
use gmars\rbac\Rbac;
use think\Route;

/**
 * Class Index
 * @package app\index\controller
 */
class Index extends base
{
    /**
     * @return \think\response\Json
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {

//        Db::name('xiaoshuo_data')->insert(['title'=>'你好']);
//        $work = new \app\worker\TimeTest();
//        $work->run();
//        $ret = Db::name('user')->find();


//        $client = new Client();
//        $res = $client->request('POST', 'http://0.0.0.0:9501/re', [
//            'debug' => false,
//            'query' => ['foo' => 'bar']
//        ]);
//        dump($res->getHeader('Set-Cookie'));
//        $body = $res->getBody();
//        $data = $body->getContents();

        $rbac = new Rbac();
//        $rbac->createTable();


//        $ret = $rbac->savePermissionCategory([
//            'name' => '用户管理组',
//            'description' => '网站用户的管理',
//            'status' => 1
//        ]);
        //dump($ret);//{ ["name"] => string(15) "用户管理组" ["description"] => string(21) "网站用户的管理" ["status"] => int(1) ["id"] => string(1) "4" }


//        $ret = $rbac->createPermission([
//            'name' => '文章列表查询',
//            'description' => '文章列表查询',
//            'status' => 1,
//            'type' => 1,
//            'category_id' => 1,
//            'path' => 'article/content/list',
//        ]);
//        dump($ret);
//        array(8) {
//                ["name"] => string(18) "文章列表查询"
//                ["description"] => string(18) "文章列表查询"
//                ["status"] => int(1)
//                ["type"] => int(1)
//                ["category_id"] => int(1)
//                ["path"] => string(20) "article/content/list"
//                ["path_id"] => string(32) "d960f3c54713200775b25d687f63eba0"
//                ["id"] => string(1) "1"
//                }
//       $ret =  $rbac->createRole([
//            'name' => '内容管理员',
//            'description' => '负责网站内容管理',
//            'status' => 1
//        ], '1,2,3');

//       dump($ret);
//        array(4) {
//        ["name"] => string(15) "内容管理员"
//        ["description"] => string(24) "负责网站内容管理"
//        ["status"] => int(1)
//        ["id"] => string(1) "1"
//}

//        $ret = $rbac->assignUserRole(1, [1]);
//        dump($ret);
//        $ret = $rbac->cachePermission(1);
//        dump($ret);

//        $rt = $rbac->generateToken(1);
//        $rt = $rbac->can('article/content/list');
//        dump($rt);
//        $data= ['abc'=>123];


        $route = new Route();
$data = $route->getRuleList();


        return json(['code'=>1001,'data'=>$data,'msg'=>'success']);
//        echo 'ok';
//        return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V5.1<br/><span style="font-size:30px">12载初心不改（2006-2018） - 你值得信赖的PHP框架111</span></p></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=64890268" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="eab4b9f840753f8e7"></think>';
//        return view();
    }

    /**
     * @param Request $request
     *
     * @return \think\response\Json
     */
    public function respon(Request $request){
        $all = $request::param();
        return json(['code'=>1001,'data'=>$all,'msg'=>'success']);
    }

    public function hello(Request $request)
    {
        $name = $request->param('username');

        $pwd = $request->param('pwd');
        //
        ////        Cache::set('login_num',0);
        //        $num = Cache::get('login_num');
        //
        //        if($num >2) {
        //            return json(['status'=>111,'msg'=>'密码尝试次数过多']);
        //        }else{
        //           if($num !== false){
        //               echo 'ssss';
        //               Cache::set('login_num',1);
        //           }else{
        //               Cache::set('login_num',$num+1);
        //           }
        //        }
        //
        //        dump($num);

        $list = Db::name('user')->where('user_name', '=', $name)->where('password', '=', $pwd)->find();

        //        $ret = Db::name('user')->insert(['user_name'=>'liu','password'=>1,'mobile'=>15040249823]);
        //
        //        if($ret != false) {
        //            echo 'success';
        //        }else{
        //            echo 'error';
        //        }


        if ($list != false) {
            return json(['status' => 1, 'msg' => '成功']);
        } else {
            return json(['status' => 110, 'msg' => '没有这个用户', [$name, $pwd]]);
        }


    }

    public function upimg()
    {
        $file = request()->file('file');
        // 移动到框架应用根目录/uploads/ 目录下
        $info = $file->move('../uploads');
        if ($info) {
            // 成功上传后 获取上传信息
            // 输出 jpg
            echo $info->getExtension();
            // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
            echo $info->getSaveName();
            // 输出 42a79759f284b767dfcb2a0197904287.jpg
            echo $info->getFilename();
        } else {
            // 上传失败获取错误信息
            echo $file->getError();
        }
    }

    public function test_db(){
        $mo = new \app\admin\model\PermissionCategory();
        Debug::remark('begin');
        $list = $mo->order("bpath asc,sort desc")->field("id,pid,name as title,sort,status,url,icon,concat(path,'-',id) as bpath")->select()->toArray();
        Debug::remark('end');
        return Debug::getRangeTime('begin','end').'s';
    }

}
